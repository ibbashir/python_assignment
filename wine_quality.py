'''
Created on Oct 13, 2018

@author: ibrahim
'''

import json
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, f1_score

def result():
    #reading into pandas dataframe
    wine = pd.read_csv('/home/ibrahim/Downloads/winequality.csv')
    
    #debugging
    #wine.head()
    #wine.info()
    #print(max(wine['quality']))
    #print(min(wine['quality']))
    
    #binning
    bins = (2, 4, 6, 7, 9)
    group_names = ['bad', 'average', 'good', 'excellent']
    wine['quality'] = pd.cut(wine['quality'], bins = bins, labels = group_names)
    
    #labelling
    label_quality = LabelEncoder()
    wine['quality'] = label_quality.fit_transform(wine['quality'])
    
    #print(wine['quality'].value_counts())
    
    #response variable - all besides quality
    X = wine.drop('quality', axis = 1)
    
    #feature variable
    y = wine['quality']
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, 
        test_size = 0.2, random_state = np.random.RandomState())
    
    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.fit_transform(X_test)
    
    rfc = RandomForestClassifier(n_estimators=200)
    rfc.fit(X_train, y_train)
    pred_rfc = rfc.predict(X_test)
    
    svc = SVC()
    svc.fit(X_train, y_train)
    pred_svc = svc.predict(X_test)
    
    print(classification_report(y_test, pred_rfc))
    print(classification_report(y_test, pred_svc))

    json_string = """
    {
        "Results": [
            {
                "dataset": "Wine Quality",
                "method": "Random Forest",
                "accuracy": 0
            },
            {
                "dataset": "Wine Quality",
                "method": "Support Vector Machine",
                "accuracy": 0
            }
        ]
    }
    """
    print(f1_score(y_test, pred_rfc, average='weighted'))
    print(f1_score(y_test, pred_svc, average='weighted'))
    
    data = json.loads(json_string)
    data["Results"][0]["accuracy"] = f1_score(y_test, pred_rfc, average='weighted')
    data["Results"][1]["accuracy"] = f1_score(y_test, pred_svc, average='weighted')
    
    return data
