# -*- coding: UTF-8 -*-
"""
hello_flask: First Python-Flask webapp
"""
import wine_quality
import titanic_disaster
from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class wineResults(Resource):
    def get(self):
        return {'data': wine_quality.result()}
    
class titanicResults(Resource):
    def get(self):
        return {'data': titanic_disaster.result()}

api.add_resource(wineResults, '/winequality/result/')
api.add_resource(titanicResults, '/titanicdisaster/result/')

if __name__ == '__main__':
     app.run()
