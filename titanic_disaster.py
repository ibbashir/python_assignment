'''
Created on Oct 13, 2018

@author: ibrahim
'''

from __future__ import division

import json
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB

def result():
    #reading into pandas dataframe
    titanic = pd.read_csv("/home/ibrahim/Downloads/titanic_disaster.csv")
    
    # Normalizing data
    titanic["Sex_cleaned"]=np.where(titanic["Sex"]=="male", 0, 1)
    # Removing nulls
    titanic=titanic[[
        "Survived",
        "Pclass",
        "Sex_cleaned",
        "Age",
        "SibSp",
        "Parch",
        "Fare"
    ]].dropna(axis=0, how='any')
    
    #debugging
    #print(titanic)
    
    # Split dataset in training and test datasets
    X_train, X_test = train_test_split(titanic, test_size=0.2, random_state= np.random.RandomState())
    
    # Gaussian Naive Bias Classifier =
    gnb = GaussianNB()
    features =["Pclass","Sex_cleaned","Age","SibSp","Parch","Fare"]
    gnb.fit(X_train[features].values, X_train["Survived"])
    y_pred = gnb.predict(X_test[features])
    
    #print(y_pred)
    
    totalpoints  = X_test.shape[0]
    mislabeled = (X_test["Survived"] != y_pred).sum()
    accuracy = (totalpoints - mislabeled)/totalpoints * 100
    print(accuracy)
    
    json_string = """
    {
        "Results": [
            {
                "dataset": "Titanic Disaster",
                "method": "Naive Bias Classifier",
                "accuracy": 0
            }
        ]
    }
    """
    
    data = json.loads(json_string)
    data["Results"][0]["accuracy"] = accuracy
    
    return data
